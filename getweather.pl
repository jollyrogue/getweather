#!/usr/bin/env perl
use strict;
use warnings;
use 5.30.0;

use LWP::Simple qw(get);
use JSON;
use Data::Dumper;

sub decodeJSON {
    return JSON->new->utf8->decode(shift @_);
}

sub getURL {
    my ($target_url) = @_;
    my $content = get($target_url);
    if (not defined $content) {
        die "Unable to retrieve $target_url";
    }
    return decodeJSON($content)
}

my $insecure = 0;
my $protocol = "https";
my $uri = "api.openweathermap.org/data/2.5/weather";
my ($apikey, $zipcode, $country) = @ARGV;

if (not defined $apikey) {
    die "API key not provided.\n";
}

if (not defined $zipcode) {
    die "Zip code not provided.\n";
}

if (not defined $country) {
    die "Country not provided.\n";
}

print("API Key: $apikey\nZip Code: $zipcode,$country\n");

if ($insecure) {
    $protocol = "http";
    print("Connection is using HTTP instead of HTTPS.\n")
}

my $url = "$protocol://$uri?zip=$zipcode,$country&appid=$apikey";
print(Dumper(getURL($url)))
